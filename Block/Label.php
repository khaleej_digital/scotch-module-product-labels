<?php
namespace Redbox\ProductLabels\Block;

use Magento\Framework\View\Element\Template;
use Redbox\ProductLabels\Helper\Label as LabelHelper;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Redbox\ProductLabels\Model\Config\Source\LabelType;

class Label extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Redbox_ProductLabels::label.phtml';

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var LabelHelper
     */
    protected $labelHelper;

    /**
     * Label constructor.
     * @param Template\Context $context
     * @param LabelHelper $labelHelper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        LabelHelper $labelHelper,
        array $data = []
    ) {
        $this->labelHelper = $labelHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function hasLabel()
    {
        if ($this->product === false)
            return false;
        return $this->labelHelper->hasLabel($this->product);
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        $image = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM_IMAGE);
        if ($image !== null) {
            $image = $image->getValue();
        } else {
            //TODO: return placeholder if image does not exists
            return '';
        }
        return $this->labelHelper->getImageUrl($image);
    }

    /**
     * @return string
     */
    public function getLabelType()
    {
        if ($labelNew = $this->product->getCustomAttribute(LabelHelper::LABEL_NEW)) {
            if ($labelNew->getValue() == Boolean::VALUE_YES) {
                return 'new';
            }
        }
        if ($labelSale = $this->product->getCustomAttribute(LabelHelper::LABEL_SALE)) {
            if ($labelSale->getValue() == Boolean::VALUE_YES) {
                return 'sale';
            }
        }
        if ($labelCustom = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM)) {
            if ($labelCustom->getValue() == Boolean::VALUE_YES) {
                $result = '';

                if ($customLabelType = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM_TYPE)) {
                    $customLabelType = $customLabelType->getValue();

                    switch ($customLabelType) {
                        case LabelType::TYPE_HTML:
                            $result = 'custom_text';
                            break;
                        case LabelType::TYPE_IMAGE:
                            $result = 'custom_image';
                            break;
                    }
                }

                return $result;
            }
        }
    }
    /**
     * @return string
     */
    public function getBadgeClass()
    {
        if ($labelNew = $this->product->getCustomAttribute(LabelHelper::LABEL_NEW)) {
            if ($labelNew->getValue() == Boolean::VALUE_YES) {
                return 'new';
            }
        }
        if ($labelSale = $this->product->getCustomAttribute(LabelHelper::LABEL_SALE)) {
            if ($labelSale->getValue() == Boolean::VALUE_YES) {
                return 'sale';
            }
        }
        if ($labelCustom = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM)) {
            if ($labelCustom->getValue() == Boolean::VALUE_YES) {
                $result = '';

                if ($customLabelType = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM_TYPE)) {
                    $customLabelType = $customLabelType->getValue();

                    switch ($customLabelType) {
                        case LabelType::TYPE_HTML:
                            $result = 'custom';
                            break;
                        case LabelType::TYPE_IMAGE:
                            $result = 'custom image';
                            break;
                    }
                }

                return $result;
            }
        }

        return '';
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getLabelText()
    {
        return $this->labelHelper->getLabelText($this->product);
    }

    /**
     * @return string
     */
    public function getLabelStyle()
    {
        if ($redboxPlCustom = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM)) {

            if ($redboxPlCustom->getValue() == Boolean::VALUE_YES) {

                if ($redboxPlCustomType = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM_TYPE)) {
                    if ($redboxPlCustomType->getValue() == LabelType::TYPE_IMAGE) {
                        // no additional styling for image
                        return '';
                    }
                }

                // return styling for HTML label
                $color = '';
                if ($redboxPlCustomColor = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM_COLOR)) {
                    $color = "color: " . $redboxPlCustomColor->getValue() . ";";
                }
                $bgColor = '';
                if ($redboxPlCustomBg = $this->product->getCustomAttribute(LabelHelper::LABEL_CUSTOM_BG)) {
                    $bgColor = "background-color: " . $redboxPlCustomBg->getValue() . ";";
                }
                return $color . " " . $bgColor;
            }
        }

        return '';
    }
}