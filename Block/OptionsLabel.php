<?php
/**
 * @category  RedboxDigital
 * @package   Redbox_ProductLabels
 * @author    V;adimir Prutyan <vladimir.prutyan@redboxdigital.com>
 * @copyright Copyright (c) 2018 Redbox Digital (http://www.redboxdigital.com)
 */
namespace Redbox\ProductLabels\Block;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\Product;
use Redbox\ProductLabels\Helper\Label as LabelHelper;

class OptionsLabel extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Redbox_ProductLabels::options_label.phtml';

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var LabelHelper
     */
    protected $labelHelper;

    /**
     * OptionsLabel constructor.
     * @param Template\Context $context
     * @param LabelHelper $labelHelper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        LabelHelper $labelHelper,
        array $data = []
    )
    {
        $this->labelHelper = $labelHelper;
        parent::__construct($context, $data);
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product $product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    public function getOptionsLabel()
    {
        return $this->labelHelper->getOptionsLabel();
    }

}