# README #

This README would normally document whatever steps are necessary to get your application up and running.

### composer.json ###


```
#!php

"redbox/module-product-labels": "0.7.1"
```


### How to use in templates ###

1) Create helper instance


```
#!php

<?php
/**
 * @var $_helperLabel \Redbox\ProductLabels\Helper\Label
 */
$_helperLabel = $this->helper('Redbox\ProductLabels\Helper\Label');
?>
```


2) Render label for product


```
#!php

<?= $_helperLabel->renderLabel($_product); ?>
```



### Label html ###

* 'New' label: <div class="badge new"><span>New</span></div>
* 'Sale' label: <div class="badge sale"><span>New</span></div>
* 'Custom' text label: <div class="badge custom"><span>Custom text</span></div>
* 'Custom' image label: <div class="badge custom image"><img src=""/></div>