<?php
/**
 * @category  Redbox
 * @package   Redbox_ProductLabels
 * @author    Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
 * @copyright Copyright (c) 2017 Redbox Digital (http://www.redboxdigital.com)
 */
namespace Redbox\ProductLabels\Model\Config\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Store\Model\StoreManagerInterface;

class Image extends AbstractBackend
{
    const ADDITIONAL_DATA_SUFFIX = '_additional_data';

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_uploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Catalog\Model\ImageUploader
     */
    private $imageUploader;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Catalog\Model\ImageUploader $imageUploader
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Catalog\Model\ImageUploader $imageUploader
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
        $this->imageUploader = $imageUploader;
    }

    /**
     * Get uploaded image name
     *
     * @param $value
     * @return bool
     * @author Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
     */
    protected function getUploadedImageName($value)
    {
        if (!is_array($value)) {
            return false;
        }

        if (!count($value)) {
            return false;
        }

        $imageData = reset($value);

        if (!isset($imageData['name'])) {
            return false;
        }

        return $imageData['name'];
    }

    /**
     * Avoiding saving potential upload data to DB
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     */
    public function beforeSave($object)
    {
        $attributeName = $this->getAttribute()->getName();
        $value = $object->getData($attributeName);

        if ($value === false || (is_array($value) && isset($value['delete']) && $value['delete'] === true)) {
            $object->setData($attributeName, '');
        } else if ($imageName = $this->getUploadedImageName($value)) {
            $object->setData($attributeName . self::ADDITIONAL_DATA_SUFFIX, $value);
            $object->setData($attributeName, $imageName);
        }

        return parent::beforeSave($object);
    }

    /**
     * Save image
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     * @author Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
     */
    public function afterSave($object)
    {
        $value = $object->getData($this->getAttribute()->getName() . self::ADDITIONAL_DATA_SUFFIX);

        if (!$imageName = $this->getUploadedImageName($value)) {
            return $this;
        }

        try {
            $this->imageUploader->moveFileFromTmp($imageName);
        } catch (\Exception $e) {
            $this->_logger->critical($e);
        }

        return $this;
    }
}
