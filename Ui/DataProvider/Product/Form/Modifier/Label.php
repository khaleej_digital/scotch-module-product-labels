<?php
/**
 * @category  Redbox
 * @package   Redbox_ProductLabels
 * @author    Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
 * @copyright Copyright (c) 2017 Redbox Digital (http://www.redboxdigital.com)
 */
namespace Redbox\ProductLabels\Ui\DataProvider\Product\Form\Modifier;

use \Magento\Catalog\Model\Locator\LocatorInterface;
use \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use \Redbox\ProductLabels\Helper\Label as Helper;

class Label extends AbstractModifier
{
    /**#@+
     * Attribute names
     */
    const CODE_REDBOX_PRODUCT_LABELS = 'redbox-product-labels';
    const CODE_PL_CUSTOM_IMAGE = 'redbox_pl_custom_image';
    /**#@-*/

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param LocatorInterface $locator
     * @param Helper $helper
     */
    public function __construct(
        LocatorInterface $locator,
        Helper $helper
    ) {
        $this->locator = $locator;
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        $product = $this->locator->getProduct();
        $modelId = $product->getId();
        $image = $product->getData(self::CODE_PL_CUSTOM_IMAGE);
        if (!isset($data[$modelId][self::DATA_SOURCE_DEFAULT][self::CODE_PL_CUSTOM_IMAGE])
        && $image) {
            $data[$modelId][self::DATA_SOURCE_DEFAULT][self::CODE_PL_CUSTOM_IMAGE][0]['name'] = $product->getData(self::CODE_PL_CUSTOM_IMAGE);
            $data[$modelId][self::DATA_SOURCE_DEFAULT][self::CODE_PL_CUSTOM_IMAGE][0]['url'] = $this->helper->getImageUrl($image);
        }

        return $data;
    }
}