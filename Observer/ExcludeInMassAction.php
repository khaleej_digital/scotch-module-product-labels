<?php

namespace Redbox\ProductLabels\Observer;

use Magento\Framework\Event\ObserverInterface;

class ExcludeInMassAction implements ObserverInterface {
    private $exclude = [
        "redbox_pl_new",
        "redbox_pl_sale",
        "redbox_pl_custom",
        "redbox_pl_custom_type",
        "redbox_pl_custom_image",
        "redbox_pl_custom_text",
        "redbox_pl_custom_color",
        "redbox_pl_custom_bgcolor"
    ];

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $block = $observer->getEvent()->getObject();

        $list = $block->getFormExcludedFieldList();
        $list = array_merge($list, $this->exclude);
        $block->setFormExcludedFieldList($list);

        return $this;
    }
}
