<?php
namespace Redbox\ProductLabels\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '0.6.0', '<')) {
            $attributeGroupName = 'Redbox Product Labels';

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_new');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_new',
                [
                    'label' => 'Apply \'New\' label',
                    'group' => $attributeGroupName,
                    'type' => 'int',
                    'default' => 0,
                    'input' => 'select',
                    'source' => '\Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 1,
                    'user_defined' => true,
                    'required' => false,
                    'used_for_promo_rules' => true,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_sale');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_sale',
                [
                    'label' => 'Apply \'Sale\' label',
                    'group' => $attributeGroupName,
                    'type' => 'int',
                    'default' => 0,
                    'input' => 'select',
                    'source' => '\Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 2,
                    'user_defined' => true,
                    'required' => false,
                    'used_for_promo_rules' => true,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_custom');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_custom',
                [
                    'label' => 'Use custom label',
                    'group' => $attributeGroupName,
                    'type' => 'int',
                    'default' => 0,
                    'input' => 'select',
                    'source' => '\Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 3,
                    'user_defined' => true,
                    'required' => false,
                    'used_for_promo_rules' => true,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_custom_type');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_custom_type',
                [
                    'label' => 'Custom label type',
                    'group' => $attributeGroupName,
                    'type' => 'int',
                    'default' => 0,
                    'input' => 'select',
                    'source' => '\Redbox\ProductLabels\Model\Config\Source\LabelType',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 4,
                    'user_defined' => true,
                    'required' => false,
                    'used_for_promo_rules' => true,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_custom_image');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_custom_image',
                [
                    'type' => 'varchar',
                    'label' => 'Custom label image',
                    'input' => 'image',
                    'backend' => 'Redbox\ProductLabels\Model\Config\Backend\Image',
                    'required' => false,
                    'sort_order' => 5,
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'group' => $attributeGroupName,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_custom_text');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_custom_text',
                [
                    'label' => 'Custom label text',
                    'group' => $attributeGroupName,
                    'type' => 'text',
                    'input' => 'text',
                    'default' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 6,
                    'user_defined' => true,
                    'required' => false,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_custom_color');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_custom_color',
                [
                    'label' => 'Custom label color',
                    'group' => $attributeGroupName,
                    'type' => 'text',
                    'input' => 'text',
                    'default' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 7,
                    'user_defined' => true,
                    'required' => false,
                    'used_in_product_listing' => true
                ]
            );

            $eavSetup->removeAttribute(Product::ENTITY, 'redbox_pl_custom_bgcolor');
            $eavSetup->addAttribute(
                Product::ENTITY,
                'redbox_pl_custom_bgcolor',
                [
                    'label' => 'Custom label background color',
                    'group' => $attributeGroupName,
                    'type' => 'text',
                    'input' => 'text',
                    'default' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'sort_order' => 8,
                    'user_defined' => true,
                    'required' => false,
                    'used_in_product_listing' => true
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.6.4', '<')) {
            // Update custom image attribute frontend model
            $eavSetup->updateAttribute(
                Product::ENTITY,
                'redbox_pl_custom_image',
                'frontend_input',
                'redbox_product_image'
            );
        }

        if (version_compare($context->getVersion(), '0.6.6', '<')) {
            // Update custom image attribute frontend model
            $eavSetup->updateAttribute(
                Product::ENTITY,
                'redbox_pl_custom_image',
                'frontend_input',
                'fileUploader'
            );
            $eavSetup->updateAttribute(
                Product::ENTITY,
                'redbox_pl_custom_image',
                'is_visible',
                0
            );
        }

        $setup->endSetup();
    }
}