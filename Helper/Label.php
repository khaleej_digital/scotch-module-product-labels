<?php
namespace Redbox\ProductLabels\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Model\Product;
use Magento\Framework\View\LayoutFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Framework\App\Helper\Context;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class Label extends AbstractHelper
{
    /**
     * Global flag to enable labels
     */
    const CONFIG_ENABLED = 'product_labels/general/enabled';

    /**
     * Global flag to enable labels
     */
    const MORE_OPTIONS_LABEL = 'product_labels/configurable_options/more_options_text';

    /**
     * Labels
     */
    const LABEL_NEW = 'redbox_pl_new';
    const LABEL_SALE = 'redbox_pl_sale';
    const LABEL_CUSTOM = 'redbox_pl_custom';
    const LABEL_CUSTOM_TYPE = 'redbox_pl_custom_type';
    const LABEL_CUSTOM_TEXT = 'redbox_pl_custom_text';
    const LABEL_CUSTOM_COLOR = 'redbox_pl_custom_color';
    const LABEL_CUSTOM_BG = 'redbox_pl_custom_bgcolor';
    const LABEL_CUSTOM_IMAGE = 'redbox_pl_custom_image';

    /**
     * Default image upload folder
     */
    const DEFAULT_PATH = 'redbox/product_labels/custom_labels/';

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var LayoutFactory
     */
    protected $_layoutFactory;

    /**
     * @var AttributeRepositoryInterface $_productAttributeRepository
     */
    protected $_productAttributeRepository;

    /**
     * @var StockRegistryInterface
     */
    protected $stockState;

    /**
     * Label constructor.
     * @param Context $context
     * @param StoreManagerInterface $_storeManager
     * @param LayoutFactory $_layoutFactory
     * @param StockRegistryInterface $stockState
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $_storeManager,
        LayoutFactory $_layoutFactory,
        StockRegistryInterface $stockState,
        AttributeRepositoryInterface $_productAttributeRepository
    )
    {
        parent::__construct($context);
        $this->_storeManager = $_storeManager;
        $this->_layoutFactory = $_layoutFactory;
        $this->stockState = $stockState;
        $this->_productAttributeRepository = $_productAttributeRepository;
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function hasLabel(Product $product) {
        // Return false if module disabled
        if ($this->scopeConfig->getValue(self::CONFIG_ENABLED) == 0) {
            return false;
        }

        $redbox_pl_new = !is_null($product->getCustomAttribute(self::LABEL_NEW)) ?
            $product->getCustomAttribute(self::LABEL_NEW)->getValue() : null;

        $redbox_pl_sale = !is_null($product->getCustomAttribute(self::LABEL_SALE)) ?
            $product->getCustomAttribute(self::LABEL_SALE)->getValue() : null;

        $redbox_pl_custom = !is_null($product->getCustomAttribute(self::LABEL_CUSTOM)) ?
            $product->getCustomAttribute(self::LABEL_CUSTOM)->getValue() : null;

        if (
            $redbox_pl_new == Boolean::VALUE_YES ||
            $redbox_pl_sale == Boolean::VALUE_YES ||
            $redbox_pl_custom == Boolean::VALUE_YES
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getOptionsLabel() {

        return $this->scopeConfig->getValue(
            self::MORE_OPTIONS_LABEL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param Product $product
     * @return string
     */
    public function renderLabel(Product $product) {
        if (!$this->hasLabel($product))
            return "";

        $layout = $this->_layoutFactory->create();

        $block = $layout->createBlock(
            '\Redbox\ProductLabels\Block\Label'
        );

        $block->setProduct($product);

        return $block->toHtml();
    }

    /**
     * @param Product $product
     * @return string
     */
    public function renderOptionsLabel(Product $product)
    {
        if (!$this->checkAssociatedSimples($product))
            return '';

        $layout = $this->_layoutFactory->create();

        $block = $layout->createBlock(
            '\Redbox\ProductLabels\Block\OptionsLabel'
        );

        $block->setProduct($product);

        return $block->toHtml();
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function checkAssociatedSimples(Product $product)
    {
        if ($product->getTypeId() != 'configurable')
            return false;

        // Return true if more than 1 simple enabled and in stock
        $count = 0;
        $children = $product->getTypeInstance()->getUsedProducts($product);
        foreach ($children as $child) {
            $stockItem = $this->stockState->getStockItem($child->getEntityId());
            if ($stockItem->getIsInStock() && $stockItem->getQty() > 0 && $child->getStatus() == '1') {
                $count++;
                if ($count > 1) return true;
            }
        }
        return false;
    }

    /**
     * @param Product $product
     * @return \Magento\Framework\Phrase
     */
    public function getLabelText(Product $product) {
        // Get attribute values
        $redbox_pl_new = !is_null($product->getCustomAttribute(self::LABEL_NEW)) ?
            $product->getCustomAttribute(self::LABEL_NEW)->getValue() : null;

        $redbox_pl_sale = !is_null($product->getCustomAttribute(self::LABEL_SALE)) ?
            $product->getCustomAttribute(self::LABEL_SALE)->getValue() : null;

        $redbox_pl_custom = !is_null($product->getCustomAttribute(self::LABEL_CUSTOM)) ?
            $product->getCustomAttribute(self::LABEL_CUSTOM)->getValue() : null;

        // Return label text for corresponding label
        if ($redbox_pl_new == Boolean::VALUE_YES) {
            return __('New');
        }
        if ($redbox_pl_sale  == Boolean::VALUE_YES) {
            return __('Sale');
        }
        if ($redbox_pl_custom == Boolean::VALUE_YES) {
            $labelText = "";
            if ($product->getCustomAttribute(self::LABEL_CUSTOM_TEXT) !== null) {
                $labelText = $product->getCustomAttribute(self::LABEL_CUSTOM_TEXT)->getValue();
            }
            return __($labelText);
        }
    }

    /**
     * Get image URL
     *
     * @param $fileName
     * @param string $path
     * @return string
     * @author Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
     */
    public function getImageUrl($fileName, $path = self::DEFAULT_PATH) {
        $url = "";
        $file = $this->_storeManager->getStore()->getBaseMediaDir() . '/' . $path .  $fileName;
        if (file_exists($file)) {
            $url = sprintf(
                "%s%s%s",
                $this->_storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ),
                $path,
                $fileName
            );
        }
        return $url;
    }
}