<?php
/**
 * @category  Redbox
 * @package   Redbox_ProductLabels
 * @author    Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
 * @copyright Copyright (c) 2017 Redbox Digital (http://www.redboxdigital.com)
 */
namespace Redbox\ProductLabels\Plugin;

use \Magento\Catalog\Api\Data\ProductAttributeInterface;
use \Redbox\ProductLabels\Model\Config\Backend\Image;

class CustomImageDelete
{
    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    /**
     * @param \Magento\Eav\Model\Config $eavConfig
     */
    public function __construct(
        \Magento\Eav\Model\Config $eavConfig
    ) {
        $this->eavConfig = $eavConfig;
    }

    /**
     * Remove custom product label in case image was deleted
     *
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Save $subject
     * @throws \Magento\Framework\Exception\LocalizedException
     * @author Konstantin Dubovenko <konstantin.dubovenko@redboxdigital.com>
     */
    public function beforeExecute(
        \Magento\Catalog\Controller\Adminhtml\Product\Save $subject
    )
    {
        $entityType = $this->eavConfig->getEntityType(ProductAttributeInterface::ENTITY_TYPE_CODE);
        $data = $subject->getRequest()->getPostValue('product') ?: [];
        foreach ($entityType->getAttributeCollection() as $attributeModel) {
            $attributeCode = $attributeModel->getAttributeCode();
            $backendModel = $attributeModel->getBackend();
            if (isset($data[$attributeCode])) {
                continue;
            }
            if (!$backendModel instanceof Image) {
                continue;
            }
            $data[$attributeCode] = false;
        }
        $subject->getRequest()->setPostValue('product', $data);
    }
}